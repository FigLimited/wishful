<?php

use Illuminate\Database\Seeder;

class Websites extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wishful = \App\Models\Website::create([
            'name' => 'Wishful',
            'fqdn' => 'https://wishful.fig.limited'
        ]);
        $wishful->refresh();
        $wishful->categories()->attach([35, 129, 135, 137, 139, 141, 147, 149, 161, 168, 169, 170, 171, 172, 175, 179, 183, 189, 194, 198, 201, 204, 205, 206, 208]);

        $diy = \App\Models\Website::create([
            'name' => 'DIY',
            'fqdn' => 'https://diy.fig.limited'
        ]);
        $diy->refresh();
        $diy->categories()->attach([428, 464, 465, 467, 473, 474, 475, 477]);
    }
}
