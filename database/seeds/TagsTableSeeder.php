<?php

use Illuminate\Database\Seeder;
use App\Models\Tag;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $women = Tag::create(['id' => 1, 'name' => 'Women', 'slug' => 'women', 'grouping' => 1]);
        $men = Tag::create(['id' => 2, 'name' => 'Men', 'slug' => 'men', 'grouping' => 1]);
        $kids = Tag::create(['id' => 3, 'name' => 'Kids', 'slug' => 'kids', 'grouping' => 1]);
        $homeGarden = Tag::create(['id' => 4, 'name' => 'Home & Garden', 'slug' => 'home-garden', 'grouping' => 1]);

        $swimwear = Tag::create(['id' => 5, 'name' => 'Swimwear', 'slug' => 'swimwear', 'grouping' => 2]);
        $appliances = Tag::create(['id' => 6, 'name' => 'Appliances', 'slug' => 'appliances', 'grouping' => 2]);
        $skinCare = Tag::create(['id' => 7, 'name' => 'Skin Care', 'slug' => 'skin-care', 'grouping' => 2]);
        $hairCare = Tag::create(['id' => 8, 'name' => 'Hair Care', 'slug' => 'hair-care', 'grouping' => 2]);
        $clothing = Tag::create(['id' => 9, 'name' => 'Clothing', 'slug' => 'clothing', 'grouping' => 2]);

        $lingerie = Tag::create(['id' => 10, 'name' => 'Lingerie', 'slug' => 'lingerie', 'grouping' => 3]);
        $accessories = Tag::create(['id' => 11, 'name' => 'Accessories', 'slug' => 'accessories', 'grouping' => 3]);
    }
}
