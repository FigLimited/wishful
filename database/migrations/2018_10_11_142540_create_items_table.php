<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('summary', 100)->nullable();
            $table->text('description')->nullable();
            $table->string('deep_link')->nullable();
            $table->string('image')->nullable();
            $table->integer('image_width')->unsigned()->nullable();
            $table->integer('image_height')->unsigned()->nullable();
            $table->integer('brand_id')->unsigned()->nullable();
            $table->integer('aw_brand_id')->unsigned()->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('merchant_id')->unsigned()->nullable();
            $table->string('aw_product_id')->nullable();
            $table->boolean('active')->default(1);
            $table->timestamps();
            $table->index('brand_id');
            $table->index('category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
