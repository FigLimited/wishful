<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->string('aw_product_id', 50);
            $table->string('data_feed_id', 50)->nullable();
            $table->text('aw_deep_link')->nullable();
            $table->string('product_name', 256)->nullable();
            $table->string('merchant_product_id', 100)->nullable();
            $table->string('merchant_image_url', 512)->nullable();
            $table->text('description')->nullable();
            $table->string('merchant_category', 256)->nullable();
            $table->float('search_price')->nullable();
            $table->string('merchant_name', 100)->nullable();
            $table->integer('merchant_id')->unsigned()->nullable();
            $table->string('brand_name', 100)->nullable();
            $table->integer('aw_brand_id')->unsigned()->nullable();
            $table->string('category_name', 100)->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->string('aw_image_url', 1024)->nullable();
            $table->string('currency', 50)->nullable();
            $table->float('store_price')->nullable();
            $table->string('color')->nullable();
            //$table->datetime('last_updated')->nullable();
            $table->integer('item_id')->unsigned()->nullable();
            $table->timestamps();
            $table->primary('aw_product_id');
            $table->unique('item_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
