<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feeds', function (Blueprint $table) {
            $table->integer('feed_id')->unsigned();
            $table->integer('advertiser_id')->unsigned();
            $table->string('advertiser_name', 150);
            $table->string('primary_region', 150)->nullable();
            $table->string('membership_status', 150)->nullable();
            $table->string('feed_name', 150)->nullable();
            $table->string('language', 50)->nullable();
            $table->string('vertical', 150)->nullable();
            $table->datetime('last_imported')->nullable();
            $table->text('url');
            $table->boolean('poll')->default(1);
            $table->datetime('last_polled')->nullable();
            $table->timestamps();
            $table->primary('feed_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feeds');
    }
}
