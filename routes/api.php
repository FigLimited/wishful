<?php

use Illuminate\Http\Request;

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//  guest
Route::middleware(['api'])->namespace('Api')->group(function () {
    Route::post('register', 'AuthController@register')->name('register');
    // Route::get('start', 'UsersController@start')->name('start');
});

//  loggedIn
Route::middleware(['auth:api', 'api'])->namespace('Api')->group(function () {
    Route::get('items/filters', 'ItemsController@filters')->name('items.filters');
    Route::get('items/get-random/{count?}', 'ItemsController@getRandom')->name('items.getRandom');
    Route::get('items/get-items', 'ItemsController@getItems')->name('items.getItems');
    Route::post('items/{item}/swipe', 'ItemsController@swipe')->name('items.swipe');

    Route::get('users/items', 'UsersController@items')->name('users.items');
    Route::post('users/post-filters', 'UsersController@postFilters')->name('users.postFilters');

    Route::post('brands/user', 'BrandsController@user')->name('brands.user');
    Route::resource('brands', 'BrandsController');

    Route::get('me', 'AuthController@me');

    Route::post('logout', 'AuthController@logout')->name('logout');
});
