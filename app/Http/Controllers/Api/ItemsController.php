<?php

namespace App\Http\Controllers\Api;

use App\Models\Item;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        //
    }

    public function filters(Request $request)
    {
        $input = $request->all();
        $user = User::where('api_token', $request->bearerToken())->with('brands')->first();
        $userId = $user->id;

        $data = [];

        $items = Item::where('active', 1)->with('brand.users', 'category')->whereHas('brand', function($q1) use($userId) {
            $q1->where('active', 1)->with(['users']);
        })->whereHas('category', function($q1) {
            $q1->where('active', 1);
        })->get();
        Log::debug('Finished Query');

        foreach($items as $item) {
            $data['brands'][$item->brand->id] = count($item->brand->users) > 0;
            $data['categories'][$item->category->id]['info'] = $item->category;
            $data['categories'][$item->category->id]['brands'][$item->brand->id]['info'] = $item->brand;
        }
        Log::debug('Finished Array');

        return response()->json([
            'data' => $data
        ]);

        // $categories = Category::where('active', 1)->whereHas('items', function($q) {
        //     $q->select('id', 'brand_id', 'category_id')->where('active', 1)->whereHas('brand', function($q2) {
        //         $q2->where('active', 1);
        //     })->with(['brand' => function($q3) {
        //         $q3->where('active', 1);
        //     }, 'users']);
        // })->with(['items' => function($q5) {
        //     $q5->where('active', 1)->with('brand', 'users');
        // }])->orderBy('name', 'ASC')->get();

        // $categories = Category::with(['items' => function($q) {
        //     $q->select('id', 'brand_id', 'category_id')->whereHas('brand', function($q2) {
        //         $q2->where('active', 1);
        //     })->with(['brand', 'users' => function($qUser) {
        //         $qUser->select('users.id');
        //     }]);
        // }])->where('active', 1)->get()->map(function($value, $index) {
        //     if(count($value->items) > 0) {
        //         return $value;
        //     }
        // });
        // dd($categories);

        // foreach($categories as $k => $category) {
        //     $data['categories'][$category->id]['info'] = $category;
        //     $data['categories'][$category->id]['brands'] = [];
        //     foreach($category->items as $kItems => $item) {
        //         if($item->brand->active == 1) {
        //             if(!isset($data['categories'][$category->id]['brands'][$item->brand->id])) {
        //                 $data['categories'][$category->id]['brands'][$item->brand->id] = 0;
        //             }
        //             $data['categories'][$category->id]['brands'][$item->brand->id]++;
        //             $data['brands'][$item->brand->id]['info'] = $item->brand;
        //             //  put selected in here
        //             $data['brands'][$item->brand->id]['selected'] = false;
        //             if(count($item->brand->users) == 1) {
        //                 $data['brands'][$item->brand->id]['selected'] = true;
        //             }
        //         }
        //     }
        //     $data['categories'][$category->id]['brands'] = array_unique($data['categories'][$category->id]['brands']);
        // }

        // dd($data['categories']);

        // usort($data['categories'], function($a, $b)
        // {
        //     return strcmp($a['info']->name, $b['info']->name);
        // });


        // return response()->json([
        //     // 'categories' => $categories,
        //     'data' => $data
        // ]);

        // $brands = Brand::where('active', 1)->has('items')->with('items.users')->orderBy('name', 'ASC')->get()->map(function($value, $index) use($user) {
        //     if($user->brands->contains($value->id)) {
        //         $value->selected = true;
        //     } else {
        //         $value->selected = false;
        //     }

        //     return $value;
        // });
        // $categories = Category::where('active', 1)->has('items')->with('items')->orderBy('name', 'ASC')->get()->map(function($value, $index) {
        //     $value->selected = false;
        //     return $value;
        // });

        // return response()->json([
        //     'brands' => $brands,
        //     'categories' => $categories
        // ]);
    }

    /**
     * getRandom method
     * may have bearertoken or brands/categories
     */
    public function getRandom(Request $request, $count = 1)
    {
        $input = $request->all();
        $user = User::where('api_token', $request->bearerToken())->with(['brands'])->first();

        //  first check if logged in
        $result = Item::getRandom($user, $count);

        return response()->json([
            'input' => $input,
            'brands' => $result['brands'],
            'count' => $result['count'],
            'items' => $result['items'],
            'sql' => $result['sql']
        ]);
    }

    /**
     * swipe method
     * processes the users swipe
     */
    public function swipe(Item $item, Request $request)
    {
        $input = $request->all();
        $user = User::where('api_token', $request->bearerToken())->first();

        $directions = [ 'up' => 1, 'right' => 2, 'down' => 3, 'left' => 4];

        $attachment = ['direction_id' => $directions[$input['direction']]];
        if(isset($input['data']['reason_id'])) {
            $attachment['reason_id'] = $input['data']['reason_id'];
        }

        $item->users()->attach($user->id, $attachment);

        if(isset($input['data']['comment'])) {
            Comment::create([
                'item_id' => $item->id,
                'user_id' => $user->id,
                'comment' => $input['data']['comment']
            ]);
        }

        return response()->json();
    }
}
