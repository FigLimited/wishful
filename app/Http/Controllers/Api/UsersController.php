<?php

namespace App\Http\Controllers\Api;

use App\Models\Item;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  anon user only needs an api token
        $apiToken = \Uuid::generate(4)->string;
        $user = User::create();
        $user->api_token = $apiToken;
        $user->save();

        return response()->json([
            'api_token' => $apiToken
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function postFilters(Request $request)
    {
        $input = $request->all();
        $user = User::where('api_token', $request->bearerToken())->first();

        $brands = collect([]);

        foreach($input['brands'] as $brand => $value) {
            if($value) {
                $brands->push($brand);
            }
        }

        $user->brands()->sync($brands);

        return response()->json([
            'input' => $input,
            'brands' => $brands,
            // 'brandIds' => $brandIds
        ]);
    }

    public function items(Request $request)
    {
        $input = $request->all();
        $direction = $input['direction'];

        $user = User::where('api_token', $request->bearerToken())->first();

        $items = Item::with('brand', 'users')->whereHas('users', function($q) use($user, $direction) {
            $q->where('direction_id', $direction)->where('users.id', $user->id);
        })->orderBy('users[0].pivot.created_at', 'DESC')->limit(12)->get()->map(function($value, $index) {
            $value->getImage();
            return $value;
        });

        // if(!$direction) {
        //     //  first load
        //     $shared = Item::with('brand')->whereHas('users', function($q) use($userId) {
        //         $q->where('direction_id', 1)->where('users.id', $userId);
        //     })->limit(10)->get()->map(function($value, $index) {
        //         $value->getImage();
        //         return $value;
        //     });
        //     $liked = Item::with('brand')->whereHas('users', function($q) use($userId) {
        //         $q->where('direction_id', 2)->where('users.id', $userId);
        //     })->limit(10)->get()->map(function($value, $index) {
        //         $value->getImage();
        //         return $value;
        //     });
        //     $reported = Item::with('brand')->whereHas('users', function($q) use($userId) {
        //         $q->where('direction_id', 3)->where('users.id', $userId);
        //     })->limit(10)->get()->map(function($value, $index) {
        //         $value->getImage();
        //         return $value;
        //     });
        //     $disliked = Item::with('brand')->whereHas('users', function($q) use($userId) {
        //         $q->where('direction_id', 4)->where('users.id', $userId);
        //     })->limit(10)->get()->map(function($value, $index) {
        //         $value->getImage();
        //         return $value;
        //     });
        //     // $items = $user->items->sortByDesc('created_at')->mapToGroups(function($value, $index) {
        //     //     $value->getImage();
        //     //     return [$value['pivot']['direction_id'] => $value];
        //     // });
        // } else {
        //     //  TODO
        //     $items[$direction] = Item::whereHas('users', function($q) use($userId, $direction) {
        //         $q->where('direction_id', $direction)->where('users.id', $userId);
        //     })->limit(10)->get();
        // }



        return response()->json([
            'items' => $items
        ]);
    }

    public function start()
    {
        $data = [
            'googleAnalytics' => config('app.google_analytics')
        ];

        return $data;
    }
}
