<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * logout method
     */
    public function logout(Request $request)
    {
        // $input = $request->all();
        // $user = User::where('api_token', $request->bearerToken())->first();
    }

    /**
     * register
     */
    public function register(Request $request)
    {
        //  random password
        $password = str_random(8);

        //  get a new User
        $user = User::register($password);

        return response()->json([
            'user' => $user,
            'password' => $password
        ], 200);
    }

    /**
     * me
     */
    public function me(Request $request)
    {
        $user = User::where('api_token', $request->bearerToken())->first();

        return response()->json([
            'user' => $user
        ]);
    }
}
