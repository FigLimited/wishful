<?php

namespace App\Http\Controllers\Api;

use App\Models\Tag;
use App\Models\Item;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::where('active', 1)->withCount('items')->orderBy('name', 'ASC')->get();

        return response()->json([
            'tags' => $tags
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        //
    }

    public function filters(Request $request)
    {
        $input = $request->all();
        $user = User::where('api_token', $request->bearerToken())->with('brands')->first();
        $userId = $user->id;

        $data = [];

        $items = Item::where('active', 1)->with('brand.users', 'tags')->whereHas('brand', function($q1) use($userId) {
            $q1->where('active', 1)->with(['users']);
        })->whereHas('tags', function($q1) {
            $q1->where('active', 1);
        })->get();
        Log::debug('Finished Query');

        foreach($items as $item) {
            foreach($item->tags as $tag) {
                $data['tags'][$tag->id]['info'] = $tag;
                $data['tags'][$tag->id]['brands'][$item->brand->id] = $item->brand;
            }
            $data['brands'][$item->brand->id] = count($item->brand->users) > 0;
        }
        Log::debug('Finished Array');

        return response()->json([
            'data' => $data,
            // 'items' => $items
        ]);
    }

    public function updateUserBrands(Request $request)
    {
        $user = User::where('api_token', $request->bearerToken())->with('brands')->first();

        $input = $request->all();

        if($input['tag'] == 0) {

        } else {
            // $user->brands()->attach()
        }
    }
}
