<?php

namespace App\Http\Controllers\Api;

use App\Models\Section;
use App\Models\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function show(Section $section)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Section $section)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function destroy(Section $section)
    {
        //
    }

    /**
     * byParent
     */
    public function byParent(Request $request, Section $parent)
    {
        $input = $request->all();

        // $descendants = Section::whereNull('parent_id')->withCount('items')->with(['items' => function($q) {
        //     $q->inRandomOrder()->first();
        // }])->get();
        $parent->load('children');
        foreach($parent->children as &$descendant) {
            $sectionId = $descendant->id;
            $item = Item::whereHas('sections', function($q) use($sectionId) {
                $q->where('sections.id', $sectionId)->limit(1);
            })
            ->inRandomOrder()->limit(1)->get();
            $descendant->item = $item;
        }

        
        // foreach($descendants as &$v) {
        //     $v->items->getImage();
        //     // $parent->descendants->items[$k]->getImage();
        // }
    
        return response()->json([
            'parent' => $parent,
            'input' => $input
        ]);
    }
}
