<?php

namespace App\Http\Controllers\Api;

use App\Models\Brand;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::where('api_token', $request->bearerToken())->first();
        $userId = $user->id;

        $brands = Brand::where('active', 1)
            ->with(['users' => function($q) use($userId) {
                $q->where('users.id', $userId);
            }])
            ->orderBy('name', 'ASC')
            ->get()
            ->map(function($value, $index) {
                if(count($value->users) == 1) {
                    $value->selected = true;
                } else {
                    $value->selected = false;
                }

                //  check for image
                if($value->logo == null) {
                    $value->logo = 'figlimited.png';
                }
                return $value;
            })
            ;

        return response()->json([
            'brands' => $brands
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        //
    }

    public function user(Request $request)
    {
        $input = $request->all();

        $user = User::where('api_token', $request->bearerToken())->with(['brands'])->first();
        $user->brands()->sync($input['brands']);

        //  first check if logged in

        $result = \App\Models\Item::getRandom($user, $input['quantity']);

        // if($result['count'] == 0) {
        //     //  no items left!!
        //     return response()->json([], 501);
        // }

        return response()->json([
            'brands' => $result['brands'],
            'count' => $result['count'],
            'items' => $result['items'],
            'sql' => $result['sql']
        ]);
    }
}
