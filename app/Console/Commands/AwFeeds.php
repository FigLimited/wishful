<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\AwTrait;

class AwFeeds extends Command
{
    use AwTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aw:feeds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get list of feeds from AW';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->getFeeds();
    }
}
