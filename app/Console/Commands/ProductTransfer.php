<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\ProductTrait;

use Illuminate\Support\Facades\Log;

class ProductTransfer extends Command
{
    use ProductTrait;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:transfer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug('Starting Transfer');
        $this->transferToItems();
    }
}
