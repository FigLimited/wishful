<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\AwTrait;

class AwFeed extends Command
{
    use AwTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aw:feed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grab a Merchant and get its products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->getFeed();
    }
}
