<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Section extends Model
{
    use NodeTrait;

    protected $guarded = [];

    public function items()
    {
        return $this->belongsToMany(Item::class, 'items_sections');
    }

}
