<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AwBrand extends Model
{
    protected $guarded = [];

    public function brand()
    {
        return $this->hasOne(Brand::class);
    }
}
