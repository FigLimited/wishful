<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reason extends Model
{
    public function items_users()
    {
        return $this->hasMany(ItemUser::class);
    }
}
