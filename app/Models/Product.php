<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    protected $guarded = [];

    protected $primaryKey = 'aw_product_id'; // or null

    public $incrementing = false;

    public function aw_brand()
    {
        return $this->belongsTo(AwBrand::class);
    }

    public function item() {
        return $this->belongsTo(Item::class);
    }

    /**
     * static method getFeed
     */
    public static function getFeed($feed)
    {
        // see if this is useable
        try {
            $fp = gzfile($feed->url);
        } catch(\Exception $e) {
            $feed->poll = 0;
            $feed->save();
            return false;
        }

        //  save feed to file
        Storage::disk('local')->put('/feeds/'.$feed->feed_id.'.csv', $fp);

        $array = [];
        // get array values
        foreach($fp as $line) {
            $array[] = str_getcsv($line);
        }
        $headers = array_shift($array);

        foreach($array as $object) {
            if(count($headers) == count($object)) {
                try {
                    $new = array_combine($headers, $object);
                    $product = Product::firstOrCreate([
                        'aw_product_id' => $new['aw_product_id']
                    ], [
                        'data_feed_id' => (isset($new['data_feed_id'])) ? $new['data_feed_id'] : null,
                        'aw_deep_link' => $new['aw_deep_link'],
                        'product_name' => $new['product_name'],
                        'merchant_product_id' => $new['merchant_product_id'],
                        'merchant_image_url' => $new['merchant_image_url'],
                        'description' => isset($new['description']) ? $new['description'] : null,
                        'currency' => (isset($new['currency'])) ? $new['currency'] : null,
                        'store_price' => (isset($new['store_price'])) ? round($new['store_price'], 2) : null,
                        'search_price' => isset($new['search_price']) ? round($new['search_price'], 2) : null,
                        'merchant_name' => $new['merchant_name'],
                        'merchant_id' => $new['merchant_id'],
                        'category_name' => $new['category_name'],
                        'category_id' => $new['category_id'],
                        'brand_name' => $new['brand_name'],
                        'aw_brand_id' => $new['brand_id'],
                        'color' => isset($new['color']) ? $new['color'] : null
                    ]);
                } catch(\Exception $e) {
                    echo $e->getMessage().PHP_EOL;
                }

                //  brand
                if($new['brand_id'] == 0 && $new['brand_name'] != '') {
                    $brand = AwBrand::firstOrCreate(['name' => $new['brand_name']]);
                    $product->aw_brand_id = $brand->id;
                    $product->save();
                } elseif($new['brand_name'] != '') {
                    AwBrand::firstOrCreate(['id' => $new['brand_id']], ['name' => $new['brand_name']]);
                }

                //  category
                if($new['category_id'] == 0 && $new['category_name'] != '') {
                    $category = Category::firstOrCreate(['name' => $new['category_name']]);
                    $product->category_id = $category->id;
                    $product->save();
                } elseif($new['category_name'] != '') {
                    $category = Category::firstOrCreate(['id' => $new['category_id']], ['name' => $new['category_name']]);
                }
            }
        }
        $feed->last_polled = date('Y-m-d G:i:s');
        $feed->save();
    }

    public function transferToItem()
    {
        $this->load('aw_brand.brand');
        if($this->aw_brand != null && $this->aw_brand->brand == null) {
            $brand = \App\Models\Brand::create([
                'aw_brand_id' => $this->aw_brand->id,
                'name' => $this->aw_brand->name
            ]);
            $this->aw_brand->refresh();
        }
        $item = \App\Models\Item::create([
            'name' => $this->product_name,
            'summary' => $this->product_short_description,
            'deep_link' => $this->aw_deep_link,
            'description' => $this->description,
            'aw_product_id' => $this->aw_product_id,
            'brand_id' => ($this->aw_brand != null) ? $this->aw_brand->brand->id : null,
            'category_id' => $this->category_id,
            'merchant_id' => $this->merchant_id,
        ]);

        $this->item_id = $item->id;
        $this->save();

        $item->copyImage();

    }
}
