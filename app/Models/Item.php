<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Exception;
use Illuminate\Support\Facades\Log;

class Item extends Model
{
    protected $guarded = [];

    // protected $appends = ['binary'];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'aw_product_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'items_users')->using(ItemUser::class)->withPivot('direction_id', 'reason_id', 'created_at')->withTimestamps();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'items_tags')->withTimestamps();
    }

    public function getBinaryAttribute()
    {
        return $this->getImage();
    }

    /**
     * getImage method
     */
    public function getImage()
    {
        \Log::info(Storage::disk('spaces')->get(config('filesystems.disks.spaces.folder').'/items/'.$this->image));
        try {
            $image = Storage::disk('spaces')->get(config('filesystems.disks.spaces.folder').'/items/'.$this->image);
            // $image = false;
            // Log::debug($this->id);

            //  work out dimensions
            // $meta   = getimagesizefromstring($image);

            //  $image2 is a resource
            // $image2 = imagecreatefromstring($image);
            // $image2 = $this->resizeImage($image, 400, 400);

            //  see if needs reducing

            // $binary = base64_encode($image);
            // $this->width = $meta[0];
            // $this->height = $meta[1];
            // $this->binary = $binary;
        } catch(\Exception $e) {
            $image = Storage::disk('spaces')->get(config('filesystems.disks.spaces.folder').'/static/figlimited.png');
        }
        $binary = base64_encode($image);
        $this->binary = $binary;
        return $binary;
    }

    /**
     * method to get image from Product
     */
    public function copyImage()
    {
        $this->load('product');
        $image = $this->product->merchant_image_url;

        if($image == '' || $this->get_http_response_code($image) != "200") {
            return false;
        }

        try {
            $content = file_get_contents($image);
        } catch(ErrorException $e) {
            Log::debug('ERROR: '.$e->getMessage());
            return false;
        }

        $size = getimagesize($image);
        $extension = image_type_to_extension($size[2]);
        if($extension != '') {
            $name = \Uuid::generate(4)->string.$extension;
            if(Storage::disk('spaces')->put(config('filesystems.disks.spaces.folder').'/items/'.$name, $content)) {
                $this->image = $name;
                $this->save();
            }
        } else {
            $this->active = 0;
            $this->save();
        }


        return $this->image;
    }

    /**
     * getRandom method
     * get random item and count remaining
     */
    public static function getRandom($user, $count = 1)
    {
        $userId = $user->id;
        $brands = $user->brands->map(function($value) {
            return $value->id;
        });

        //  from filters
        $items = self::where('active', 1)->with('brand.users')->whereHas('brand', function($q1) {
            $q1->where('active', 1);
        })->whereDoesntHave('users', function($q3) use($userId) {
            $q3->where('users.id', $userId);
        })
        ->whereIn('brand_id', $brands)->inRandomOrder();

        $returned = $items->count();

        $array = $items->limit($count)->get();
        foreach($array as &$a) {
            $a->getImage();
        }

        return [
            'brands' => $brands,
            'items' => $array,
            'sql' => $items->toSql(),
            'count' => $returned
        ];
    }

    private function get_http_response_code ($url)
    {
        $headers = get_headers($url);
        return substr($headers[0], 9, 3);
    }

    private function resizeImage($file, $w, $h, $crop=FALSE) {
        list($width, $height) = getimagesizefromstring($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width-($width*abs($r-$w/$h)));
            } else {
                $height = ceil($height-($height*abs($r-$w/$h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w/$h > $r) {
                $newwidth = $h*$r;
                $newheight = $h;
            } else {
                $newheight = $w/$r;
                $newwidth = $w;
            }
        }
        $src = imagecreatefromstring($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        return $dst;
    }
}
