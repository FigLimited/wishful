<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GuzzleHttp\Client;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function items()
    {
        return $this->belongsToMany(Item::class, 'items_users')->using(ItemUser::class)->withPivot('direction_id', 'reason_id', 'created_at')->withTimestamps();
    }

    public function brands()
    {
        return $this->belongsToMany(Brand::class, 'brands_users')->withTimestamps();
    }

        /**
     * register method
     */
    public static function register($password)
    {
        $client = new Client();
        $result = $client->request('GET', 'https://randomuser.me/api/?inc=login');
        $json = json_decode((string)$result->getBody());
        $loginData = $json->results[0];

        //  random details to get started
        $user = self::create([
            'username' => $loginData->login->username,
            'password' => bcrypt($password),
            'api_token' => $loginData->login->uuid
        ]);

        $user->save();
        $user->refresh();

        return $user;
    }

}
