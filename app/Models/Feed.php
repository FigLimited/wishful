<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Feed extends Model
{
    protected $guarded = [];

    protected $primaryKey = 'feed_id';

    public $incrementing = false;

    /**
     * static method getFeed
     */
    public static function getFeed($feed)
    {
        // see if this is useable
        try {
            $fp = gzfile($feed->url);
        } catch(\Exception $e) {
            $feed->poll = 0;
            $feed->save();
            return false;
        }

        //  save feed to file
        Storage::disk('local')->put('/feeds/'.$feed->feed_id.'.csv', $fp);

    }

}
