<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'categories_users')->withTimestamps();
    }

    public function websites()
    {
        return $this->belongsToMany(Website::class, 'categories_websites');
    }
}
