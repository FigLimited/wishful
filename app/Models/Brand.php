<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $guarded = [];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function itemCount()
    {
        return $this->hasMany(Item::class)->count();
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'brands_users')->withTimestamps();
    }
}
