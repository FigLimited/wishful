<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ItemUser extends Pivot
{
    public function direction()
    {
        return $this->belongsTo(Direction::class);
    }

    public function reason()
    {
        return $this->belongsTo(Reason::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
