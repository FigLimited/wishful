<?php
namespace App\Traits;

use App\Models\Feed;

trait AwTrait
{
    public function getFeeds()
    {
        $passFirst = true; //  set to false if no header line

        $url = sprintf('http://datafeed.api.productserve.com/datafeed/list/apikey/%s', config('app.ps_api_key'));

        $feeds = file($url);

        $count = 0;
        foreach ($feeds as $feed) {
            if ($passFirst) {
                $passFirst = false;
                continue;
            }
            $array = str_getcsv($feed);

            Feed::updateOrCreate(
                [
                    'feed_id' => $array[4],
                ],
                [
                    'advertiser_id' => $array[0],
                    'advertiser_name' => $array[1],
                    'primary_region' => $array[2],
                    'membership_status' => $array[3],
                    'feed_id' => $array[4],
                    'feed_name' => $array[5],
                    'language' => $array[6],
                    'vertical' => $array[7],
                    'last_imported' => ($array[8] != '') ? $array[8] : null,
                    'url' => $array[11]
                ]
            );

            $count++;
        }

    }

    /**
     * getFeed method
     */
    public function getFeed()
    {
        //  next feed based on poll date
        $feed = $this->getNextFeed();
        echo $feed->advertiser_name.' - ' .$feed->feed_id.PHP_EOL;

        //  static method to process feed
        Feed::getFeed($feed);
    }

    /**
     * getNextFeed method
     */
    private function getNextFeed()
    {
      $nextFeed = Feed::where('poll', true)->orderBy('last_polled', 'ASC')->orderBy('membership_status', 'ASC')->first();
      return $nextFeed;
    }

}
