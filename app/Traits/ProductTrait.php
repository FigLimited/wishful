<?php
namespace App\Traits;

use Illuminate\Support\Facades\Log;
use App\Models\Product;

trait ProductTrait
{
    public function transferToItems()
    {
        $products = Product::whereNull('item_id')->inRandomOrder()->limit(200)->get();

        foreach($products as $product) {
            \Log::debug($product->aw_product_id.' - '.$product->merchant_image_url);
            $product->transferToItem();
        }
    }
}