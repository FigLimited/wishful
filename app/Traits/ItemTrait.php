<?php
namespace App\Traits;

use Illuminate\Support\Facades\Log;
use App\Models\Item;
use App\Models\Tag;

trait ItemTrait
{
    public function itemTag()
    {
        $tags = Tag::with('items')->get();;

        foreach($tags as $tag) {
            $items = $tag->items->map(function($value, $index) {
                return $value->id;
            })->toArray();
            // dd(count($items));
            $tag->csv_count = count($items);
            $tag->csv = implode(',', $items);
            $tag->save();
        }
    }
}