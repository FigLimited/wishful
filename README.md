# Wishful
### by Sharif Khan (Fig Limited)

A concept website to highlight VueJS and Laravel techniques.


## Installation

###### Download the repo:
`git clone https://FigLimited@bitbucket.org/FigLimited/wishful.git wishful`

###### Go into the dirctory:
`cd wishful`

###### Update the PHP dependencies:
`composer update`

###### Update the Quasar dependencies:
`cd quasar`
`npm i`

###### Build and copy the front end:
`quasar build`

`cp -r dist/spa-mat/* ../public`

###### Copy .env.example to .env and make necessary changes
`cp .env.example .env`

###### Pre-populate the database:
`php artisan migrate:refresh`

###### If you are using a local computer for development then you need to activate the local web server. The response should tell you what the url is:
`php artisan serve` or `./artisan serve`
